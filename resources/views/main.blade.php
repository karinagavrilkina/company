@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    

                    @if (count($employes) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Company
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            
                            <tbody>
                                <tr>
                                    <td>Отделы/Сотрудники</td>
                                    @foreach ($departments as $d)
                                    <td>
                                        {{ $d->name}}
                                    </td>
                                    @endforeach
                                </tr>
                                    
                                    @foreach ($employes as $e)
                                    <tr>
                                        <td class="table-text"><div><span>{{ $e->surname }}</span><span> {{ $e->name }}</span><span> {{ $e->patronymic }}</span></div></td>

                                        <!-- Task Delete Button -->
                                        
                                        @foreach ($departments as $d)
                    
                                            @if (in_array($d->id, explode(',',$e->department_id)))
                                            
                                                <td>+</td>
                                            
                                            
                                            @else
                                                <td></td>
                                            @endif
                                        @endforeach
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
            </div>

            
        </div>
    </div>
@endsection
