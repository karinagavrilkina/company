@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">


                <div class="panel-heading">
                    Добавить сотрудника
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    <form action="{{ url('addemploye')}}" method="POST" class="form-horizontal" id="feedback-form">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Имя*</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" class="form-control" value="" id='name'>
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="surname" class="col-sm-3 control-label">Фамилия*</label>

                            <div class="col-sm-6">
                                <input type="text" name="surname" class="form-control" value="" id='surname'>
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patronymic" class="col-sm-3 control-label">Отчество</label>

                            <div class="col-sm-6">
                                <input type="text" name="patronymic" class="form-control" value="" id='patronymic'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-sm-3 control-label">Пол</label>
                            <div class="col-sm-6">
                                <select name="gender" class="form-control">
                                    <option value='м'>м</option>
                                    <option value='ж'>ж</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="salary" class="col-sm-3 control-label">Зп*</label>

                            <div class="col-sm-6">
                                <input type="text" name="salary" class="form-control" value="" id='salary'>
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="department" class="col-sm-3 control-label">Отдел*</label>

                            <div class="col-sm-6">
                                <select name="department" class="form-control" multiple size='3'>
                                    @foreach ($departments as $d)
                                    <option value='{{$d->id}}'>{{$d->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default" id='send'>
                                    <i class="fa fa-btn fa-plus"></i>Добавить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($employes) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Сотрудники
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            <thead>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Отчество</th>
                                <th>Пол</th>
                                <th>Зп</th>
                                <th>Отдел</th>
                            </thead>
                            <tbody>
                                @foreach ($employes as $e)
                                    <tr>
                                        <td class="table-text">{{ $e->surname }}</td>
                                        <td class="table-text">{{ $e->name }}</td>
                                        <td class="table-text">{{ $e->patronymic }}</td>
                                        <td class="table-text">{{ $e->gender }}</td>
                                        <td class="table-text">{{ $e->salary }}</td>
                                        @foreach ($departments as $d)

                                        @if (in_array($d->id, explode(',',$e->department_id)))
                                        <td class="table-text" style='display:inline-block;'>{{ $d->name }}</td>
                                        @endif
                                        @endforeach
                                        
                                        <td>
                                            <form action="{{ url('employe/'.$e->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="{{ url('editemploye/'.$e->id) }}" method="POST">
                                                {{ csrf_field() }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Edit
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src='..\resources\views\employes.js'></script>
    <link rel="stylesheet" href="..\resources\views\style.css">
@endsection

