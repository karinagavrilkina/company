@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@if (count($departments) == 1)
                <div class="panel-heading">
                    Изменить отдел
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                     <form action="{{ url('updatedepartment/'.$departments[0]->id)}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Название*</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" class="form-control" id='name' value="{{ $departments[0]->name }}">
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="count_employe" class="col-sm-3 control-label">Кол-во сотрудников</label>

                            <div class="col-sm-6">
                                <input type="text" name="count_employe" class="form-control" value="{{ $departments[0]->count_employe }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="max_salary" class="col-sm-3 control-label">Максимальная зп</label>

                            <div class="col-sm-6">
                                <input type="text" name="max_salary" class="form-control" value="{{ $departments[0]->max_salary }}">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
<script type="text/javascript" src='..\resources\views\departments.js'></script>
@endif
@if (count($employes) == 1)
                <div class="panel-heading">
                    Изменить сотрудника
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                     <form action="{{ url('updateemploye/'.$employes[0]->id)}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                       <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Имя*</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id='name' class="form-control" value="{{$employes[0]->name}}">
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="surname" class="col-sm-3 control-label">Фамилия*</label>

                            <div class="col-sm-6">
                                <input type="text" name="surname" class="form-control" id='surname' value="{{$employes[0]->surname}}">
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patronymic" class="col-sm-3 control-label">Отчество</label>

                            <div class="col-sm-6">
                                <input type="text" name="patronymic" class="form-control" value="{{$employes[0]->patronymic}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-sm-3 control-label">Пол</label>
                            <div class="col-sm-6">
                                <select name="gender" class="form-control">
                                    <option value='м' @if ($employes[0]->gender=='м'){{'selected' }}@endif>м</option>
                                    <option value='ж' @if ($employes[0]->gender=='ж'){{'selected'}} @endif>ж</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="salary" class="col-sm-3 control-label">Зп*</label>

                            <div class="col-sm-6">
                                <input type="text" name="salary" class="form-control" id='salary' value="{{$employes[0]->salary}}">
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="department_id" class="col-sm-3 control-label">Отдел*</label>

                            <div class="col-sm-6">
                                <select name="department" class="form-control" multiple size='3'>
                                    

                                    @foreach ($departments as $d)
                                    <option value='{{$d->id}}' @if (in_array($d->id, explode(',',$employes[0]->department_id))){{'selected'}}@endif>{{$d->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

<script type="text/javascript" src='../../resources/views/employes.js'></script>
@endif
            </div>
        </div>
    </div>
    
    
    <link rel="stylesheet" href="../../resources/views/style.css">
@endsection
