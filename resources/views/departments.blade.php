@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">


                <div class="panel-heading">
                    Добавить отдел
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                     <form action="{{ url('adddepartment')}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Название*</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" class="form-control" value="" id='name'>
                                <div class="error-box"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="count_employe" class="col-sm-3 control-label">Кол-во сотрудников</label>

                            <div class="col-sm-6">
                                <input type="text" name="count_employe" class="form-control" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="max_salary" class="col-sm-3 control-label">Максимальная зп</label>

                            <div class="col-sm-6">
                                <input type="text" name="max_salary" class="form-control" value="">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default" id='send'>
                                    <i class="fa fa-btn fa-plus"></i>Добавить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($departments) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Отделы
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            <thead>
                                <th>Название</th>
                                <th>Кол-во сотрудников</th>
                                <th>Максимальная зп</th>
                            </thead>
                            <tbody>
                                @foreach ($departments as $d)
                                    <tr>
                                        <td class="table-text">{{ $d->name }}</td>
                                        <td class="table-text">{{ $d->count_employe }}</td>
                                        <td class="table-text">{{ $d->max_salary }}</td>
                                        
                                        <td>
                                            <form action="{{ url('deldepartment/'.$d->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="{{ url('editdepartment/'.$d->id) }}" method="POST">
                                                {{ csrf_field() }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Edit
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src='..\resources\views\departments.js'></script>
    <link rel="stylesheet" href="..\resources\views\style.css">
@endsection
