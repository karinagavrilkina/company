<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use App\Department;
use App\Employe;
use Illuminate\Http\Request;

Route::group(['middleware' => ['web']], function () {
    /**
     * Show main page
     */

    Route::get('/', function () {
        return view('main', [
            'employes' => Employe::get(),
            'departments' => Department::get()
        ]);
    });

    Route::post('/form', function (Request $request) {
    if($request->input('flag')=='Отделы'||$request->input('flag')=='Сотрудники'){
        $flag=$request->input('flag');
        return redirect('/'.$flag);
    }
    });


    /**
     * Show employe
     */

    Route::get('/Сотрудники', function () {
        return view('employes', [
            'employes' => Employe::orderBy('name', 'asc')->get(),
            'departments' => Department::orderBy('name', 'asc')->get()
        ]);
    });

    /**
     * Add New employe
     */
    Route::post('/addemploye', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'surname' => 'required|max:100',
            'patronymic' => 'required|max:100',
            'gender' => 'required|max:10'
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $employe = new Employe;
        $employe->name = $request->name;
        $employe->surname = $request->surname;
        $employe->patronymic = $request->patronymic;
        $employe->gender = $request->gender;
        $employe->salary = $request->salary;
        $employe->department_id = implode(',', $request->department);
        $employe->save();

        return redirect('/');
    });

    /**
     * Delete employe
     */
    Route::delete('/delemploye/{id}', function ($id) {
        Employe::findOrFail($id)->delete();

        return redirect('/');
    });

    Route::post('/updateemploye/{id}', function ($id,Request $request) {
        DB::table('employes')->where('id',$id)->update(['name'=>$request->input('name'),
            'surname'=>$request->input('surname'),
            'patronymic'=>$request->input('patronymic'),
            'gender'=>$request->input('gender'),
            'salary'=>$request->input('salary'),
            'department_id'=>$request->input('department')
    ]);
        return redirect('/');
    });

    Route::post('/editemploye/{id}', function ($id) {
        return view('edit', [
            'employes' => Employe::where('id',$id)->get(),
            'departments' => Department::orderBy('name', 'asc')->get()
        ]);

    });









    /**
     * Show departments
     */
    
    Route::get('/Отделы', function () {
        return view('departments', [
            'departments' => Department::orderBy('name', 'asc')->get()
        ]);
    });

    /**
     * Add New User
     */
    Route::post('/adddepartment', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $department = new Department;
        $department->name = $request->name;
        $department->count_employe = $request->count_employe;
        $department->max_salary = $request->max_salary;
        $department->save();

        return redirect('/');
    });

    /**
     * Delete User
     */
    Route::delete('/deldepartment/{id}', function ($id) {
        Department::findOrFail($id)->delete();

        return redirect('/');
    });


    Route::post('/updatedepartment/{id}', function ($id,Request $request) {
        DB::table('departments')->where('id',$id)->update(['name'=>$request->input('name'),
            'count_employe'=>$request->input('count_employe'),
            'max_salary'=>$request->input('max_salary')
    ]);
        return redirect('/');
    });

    Route::post('/editdepartment/{id}', function ($id) {
        return view('edit', [
            'departments' => Department::where('id',$id)->get()
        ]);
        //return redirect('/');

    });
});
